const bcrypt = require('bcrypt')
const { UserUser } = require("../../database/models")
class UserUserRep {
    constructor(username, password) {
        this.username = username
        this.password = password
    }
    async createUser() {
        const salt = await bcrypt.genSalt();
        const hashPass = await bcrypt.hash(this.password, salt);
        const newUser = await UserUser.create({
            username: this.username,
            password: hashPass,
        });
        return newUser
    };
}



module.exports = UserUserRep