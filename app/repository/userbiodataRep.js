const { UserBiodata } = require('../../database/models')
class userBiodataRep {
    constructor(email, gender, UserUserId) {
        this.email = email
        this.gender = gender
        this.UserUserId = UserUserId
    }
    async createBiodata() {
        const newBiodata = await UserBiodata.create({
            email: this.email,
            gender: this.gender,
            UserUserId: this.UserUserId,
        });
        return newBiodata
    };
}


module.exports = userBiodataRep