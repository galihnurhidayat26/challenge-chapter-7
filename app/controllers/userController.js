const { UserUser, UserBiodata, UserHistory, UserAdmin } = require('../../database/models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
require('dotenv').config()
const UserUserRep = require("../repository/useruserRep")
const userBiodataRep = require("../repository/userbiodataRep")

class userController {
    static async register(req, res) {
        const userUserRep = new UserUserRep(req.body.username, req.body.password)
        const newUser = await userUserRep.createUser();
        console.log(newUser);
        const userBiodata = new userBiodataRep(
            req.body.email,
            req.body.gender,
            newUser.id
        )
        const newBiodata = await userBiodata.createBiodata()
        console.log(newBiodata)

    }
    static renderRegister(req, res) {
        res.render("register", { title: "Register" })
    }

    static login(req, res) {
        UserUser.findOne({
            where: { username: req.body.username }
        }).then(async (userLogin) => {
            if (userLogin === null) {
                UserAdmin.findOne({
                    where: {
                        username: req.body.username
                    }
                }).then(async (userAdmin) => {
                    if (userAdmin === null) {
                        res.send("user unregistered ")
                    }
                    else {
                        let compare = await bcrypt.compare(
                            req.body.password,
                            userAdmin.password
                        );
                        console.log("admin compare")
                        if (compare) {
                            const user = {
                                id: userAdmin.id,
                                role: "admin"
                            };
                            const token = jwt.sign(user, process.env.TOKEN_SECRET);
                            const data = {
                                token: token,
                            };
                            res.json(data);
                            console.log(`auth key:${token}`)
                            console.log("welcome admin!")
                            // req.userRole = "admin"
                            res.redirect("/dashboard")
                            // res.post("/token")
                        }
                        else {
                            console.log("wrong admin password")
                            res.send("wrong admin password")
                        }
                    }
                });
                console.log("no user")
                console.log("cannot find admin")
            } else {
                let compare = await bcrypt.compare(
                    req.body.password,
                    userLogin.password)
                console.log("user compare")
                if (compare) {
                    const user = {
                        id: userLogin.id,
                        role: "user"
                    };
                    const token = jwt.sign(user, process.env.TOKEN_SECRET);
                    const data = {
                        token: token,
                    };
                    res.json(data);
                    console.log(`auth key:${token}`)
                    console.log("Welcome user!")
                    req.userRole = "user"
                    res.redirect("/dashboard")
                }
                else {
                    console.log("wrong user password")
                    res.send("wrong user password")
                }
            }

        })
            .catch((error) => {
                console.log(error)
                res.json()
            })

    }
    static renderLogin(req, res) {
        res.render("login", { title: "Login" })
    }
    static userListing(req, res) {
        res.json(user)
    }
    static renderUpdate(req, res) {
        UserBiodata.findOne({
            where: {
                id: req.params.id
            }
        }).then((biodata) => {
            console.log(biodata);
            res.render("update", { title: "Update", biodata })
        })
    }
    static update(req, res) {
        console.log(biodata)
        UserBiodata.update({
            email: req.body.email,
        },
            {
                returning: true, where: { id: req.params.id }
            }).then((biodata) => {
                res.redirect("/dashboard")
            })
    }
    static delete(req, res) {
        UserBiodata.destroy({
            where: {
                UserUserId: req.params.id,
            }
        }).then(() => {
            UserHistory.destroy({
                where: {
                    UserUserId: req.params.id
                }
            }).then(() => {
                UserUser.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then(() => {
                    res.redirect("/dashboard")
                })
            })
        })
    }
}
module.exports = userController