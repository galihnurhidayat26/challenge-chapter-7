const GenerateStringHelper = require('../helper/generateStringHelper')
const { room } = require('../../database/models')
const { RoomGameProgress } = require('../../database/models')

class roomController {
    static async room(req, res) {
        res.status(201).json({ "message": "success created room" })
    }
    static async generateRoom(req, res) {
        const generateStringHelper = new GenerateStringHelper()
        const randomString = await generateStringHelper.generateString(10)
        room.create({
            owner: req.user.id,
            roomCode: randomString,
            room_name: req.body.room_name
        }).then((roomCreated) => {
            const data = {
                room_name: req.body.room_name,
                room_code: randomString,
                owner: req.user.id,
                id: roomCreated.id,
            };
            const response = {
                message: "success generated room code",
                data: data,
            }
            res.status(201).json(response)
        })
    }
    static joinRoom(req, res) {
        room
            .findOne({
                where: {
                    roomCode: req.body.roomCode,
                },
            })
            .then((roomFound) => {
                if (roomFound == null) {
                    res.json({ message: "cannot find room" });
                } else {
                    if (roomFound.guest == null && req.user.id !== roomFound.owner) {
                        room.update(
                            { guest: req.user.id },
                            {
                                where: {
                                    roomCode: req.body.roomCode,
                                },
                            }
                        );
                    } else {
                        res.json({ message: "room not receive participant" });
                    }
                }
            })
            .then(() => {
                res.json({ message: "guest created" });
            });
    }
    static roomPlayground(req, res) {
        room
            .findOne({
                where: { id: req.body.id },
            })
            .then((roomFound) => {
                if (roomFound == null) {
                    res.json("cannot find room");
                } else {
                    if (req.user.id == roomFound.owner) {
                        RoomGameProgress.findOne({
                            where: {
                                roomId: roomFound.id,
                            },
                        }).then((RoomIdFound) => {
                            if (RoomIdFound == null) {
                                RoomGameProgress.create({
                                    ownerChoice: req.body.choice,
                                    attempt: 0,
                                    roomId: roomFound.id
                                }).then((created) => {
                                    res.json(created)
                                    res.json({ message: `you select ${ownerChoice}` });
                                })
                            } else if (RoomIdFound && RoomIdFound.ownerChoice == null) {
                                RoomGameProgress.update({
                                    ownerChoice: req.body.choice,
                                },
                                    { where: { roomId: roomFound.id } })
                                    .then(() => {
                                        res.json({ message: "repicked!" })
                                    })
                            } else {
                                res.json({ message: "owner has been selected" })
                            }
                        });
                    }
                    else {
                        RoomGameProgress.findOne({
                            where: {
                                roomId: roomFound.id
                            }
                        }).then((RoomIdFound) => {
                            if (RoomIdFound.guestChoice == null) {
                                RoomGameProgress.update({
                                    guestChoice: req.body.choice,
                                },
                                    { where: { roomId: req.body.id } }).then(() => {
                                        RoomGameProgress.findOne({
                                            where: { roomId: roomFound.id }
                                        }).then((RoomIdFound) => {
                                            const owner = RoomIdFound.ownerChoice;
                                            const guest = RoomIdFound.guestChoice;
                                            if (owner == guest) {
                                                RoomGameProgress.update(
                                                    {
                                                        ownerChoice: null,
                                                        guestChoice: null
                                                    }, {
                                                    where: {
                                                        roomId: req.body.id
                                                    }
                                                }
                                                );
                                                res.json({
                                                    message: "Game Tied!"
                                                })
                                            }
                                            else if (owner == "gunting" && guest == "kertas") {
                                                if (RoomIdFound.attempt == null) {
                                                    RoomIdFound.attempt = 0;
                                                    RoomGameProgress.update({
                                                        attempt: +1,
                                                        result: "owner win"
                                                    },
                                                        {
                                                            where: { roomId: req.body.id }

                                                        })
                                                }
                                            }
                                            else if (owner == "gunting" && guest == "batu") {
                                                if (RoomIdFound.attempt == null) {
                                                    RoomIdFound.attempt = 0;
                                                    RoomGameProgress.update({
                                                        attempt: +1,
                                                        result: "owner lost"
                                                    },
                                                        {
                                                            where: {
                                                                roomId: req.body.id
                                                            }
                                                        })
                                                }
                                            }
                                            else if (owner == "kertas" && guest == "batu") {
                                                if (RoomIdFound.attempt == null) {
                                                    RoomIdFound.attempt = 0;
                                                    RoomGameProgress.update({
                                                        attempt: +1,
                                                        result: "owner win"
                                                    },
                                                        {
                                                            where: {
                                                                roomId: req.body.id
                                                            }
                                                        })
                                                }
                                            }
                                            res.json({ message: "winner-loser" })
                                        })
                                    })
                            } else {
                                res.json({ message: "guest not null" })
                            }
                        });
                    }
                }
            })
    }
}
module.exports = roomController