const { UserUser, UserBiodata, UserHistory } = require('../../database/models')

class dashboardController {
    static renderDashboard(req, res) {
        UserBiodata.findAll({ include: ["UserUser"] }).then((alluser) => {
            console.log(req.userRole)
            res.render("dashboard", { title: "Dashboard", alluser })
        })
    }
}

module.exports = dashboardController