var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt')
const Auth = require('../middlewares/authMiddleware')
const userController = require("../controllers/userController")

/* GET users listing. */
// router.get('/', function (req, res, next) {
//   res.send('respond with a resource');
// });

router.get('/', userController.userListing)
router.get('/login', userController.renderLogin)
router.post('/login', userController.login)
router.post('/token', userController.login)

router.get('/register', userController.renderRegister)
router.post('/register', userController.register)

router.get('/update/:id', userController.renderUpdate)
router.post('/update:id', userController.update)
router.get('/delete/:id', userController.delete)


module.exports = router;
