var express = require('express');
var router = express.Router();
const roomController = require("../controllers/roomController")

router.get('/', roomController.room)
router.post('/generate', roomController.generateRoom)
router.post("/join", roomController.joinRoom);
router.post("/playground", roomController.roomPlayground);

module.exports = router;