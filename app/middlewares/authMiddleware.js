const jwt = require("jsonwebtoken");

class authMiddleware {
    async auth(req, res, next) {
        try {
            const authHeader = req.headers['authorization']
            console.log(req.headers)
            console.log(authHeader)
            const token = authHeader && authHeader.split(" ")[1];
            jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
                if (err) {
                    return res.sendStatus(403);
                }
                req.user = user;
                next();
            });
        } catch (error) {
            return res.sendStatus(403);
        }
    }
}

module.exports = authMiddleware;
