'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  room.init({
    owner: DataTypes.INTEGER,
    guest: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    startedAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    roomCode: DataTypes.STRING,
    room_name: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'room',
  });
  return room;
};