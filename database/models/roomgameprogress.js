'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomGameProgress extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  RoomGameProgress.init({
    roomId: DataTypes.INTEGER,
    attempt: DataTypes.INTEGER,
    ownerChoice: DataTypes.STRING,
    guestChoice: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomGameProgress',
  });
  return RoomGameProgress;
};