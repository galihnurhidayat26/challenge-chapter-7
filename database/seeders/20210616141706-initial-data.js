'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("UserUsers", [
      {
        username: "admin",
        email: "admin@gmail.com",
        password: bcrypt("admin"),
        createdAt: new Date(),
        updatedAt: new Date(),
      }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
