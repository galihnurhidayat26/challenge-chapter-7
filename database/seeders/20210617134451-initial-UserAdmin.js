'use strict';
const bcrypt = require('bcrypt');
// const UserAdmin = require('../models')
const hashPass = bcrypt.hash("admin", 10);
// UserAdmin.create({
//   password: hashPass,
// })

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const salt = await bcrypt.genSalt();
    const hashPass = await bcrypt.hash("admin", salt);
    // UserUser.create({
    //   password: hashPass,
    // })
    await queryInterface.bulkInsert("UserAdmins", [
      {
        username: "adminadmin",
        email: "admin@gmail.com",
        password: hashPass,
        createdAt: new Date(),
        updatedAt: new Date(),
      }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("UserAdmins", [
      {
        username: "admin",
        email: "admin@gmail.com",
        password: "123",
        createdAt: new Date(),
        updatedAt: new Date(),
      }])
  }
};
